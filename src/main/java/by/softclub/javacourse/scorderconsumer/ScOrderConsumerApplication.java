package by.softclub.javacourse.scorderconsumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class ScOrderConsumerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ScOrderConsumerApplication.class, args);
    }

}
