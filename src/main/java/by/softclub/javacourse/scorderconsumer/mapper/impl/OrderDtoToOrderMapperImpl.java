package by.softclub.javacourse.scorderconsumer.mapper.impl;


import by.softclub.javacourse.scorderconsumer.domain.Currency;
import by.softclub.javacourse.scorderconsumer.domain.Order;
import by.softclub.javacourse.scorderconsumer.domain.enums.OrderType;
import by.softclub.javacourse.scorderconsumer.dto.OrderDto;
import by.softclub.javacourse.scorderconsumer.mapper.OrderDtoToOrderMapper;
import by.softclub.javacourse.scorderconsumer.repository.CurrencyRepository;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class OrderDtoToOrderMapperImpl implements OrderDtoToOrderMapper {

    private final CurrencyRepository currencyRepository;

    public OrderDtoToOrderMapperImpl(CurrencyRepository currencyRepository) {
        this.currencyRepository = currencyRepository;
    }

    @Override
    public Order map(OrderDto dto) {

        var order = new Order();
        order.setAmount(dto.getAmount());
        order.setFee(dto.getFee());
        order.setOrderType(OrderType.BUY);
        order.setSourceCurrency(getCurrency(dto.getSourceCurrency()));
        order.setTargetCurrency(getCurrency(dto.getTargetCurrency()));

        return order;
    }

    private Currency getCurrency(String iso) {
        Optional<Currency> optCurr = currencyRepository.findByCode(iso);
        if (optCurr.isPresent()) {
            return optCurr.get();
        } else {
            throw new RuntimeException("Currency with code " + iso + " not found");
        }
    }

}
