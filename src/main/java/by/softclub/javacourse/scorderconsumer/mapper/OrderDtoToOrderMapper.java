package by.softclub.javacourse.scorderconsumer.mapper;


import by.softclub.javacourse.scorderconsumer.domain.Order;
import by.softclub.javacourse.scorderconsumer.dto.OrderDto;

public interface OrderDtoToOrderMapper extends ObjectMapper<OrderDto, Order> {
}
