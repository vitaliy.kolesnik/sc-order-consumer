package by.softclub.javacourse.scorderconsumer.repository;


import by.softclub.javacourse.scorderconsumer.domain.Order;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Order, Integer> {
}
