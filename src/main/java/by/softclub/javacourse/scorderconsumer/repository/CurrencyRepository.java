package by.softclub.javacourse.scorderconsumer.repository;


import by.softclub.javacourse.scorderconsumer.domain.Currency;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CurrencyRepository extends JpaRepository<Currency, Integer> {
    Optional<Currency> findByCode(String code);
}
