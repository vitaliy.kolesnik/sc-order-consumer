package by.softclub.javacourse.scorderconsumer.service;

import by.softclub.javacourse.scorderconsumer.dto.OrderDto;

public interface OrderService {

    void create(OrderDto dto);

}
