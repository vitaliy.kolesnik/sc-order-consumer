package by.softclub.javacourse.scorderconsumer.service.impl;

import by.softclub.javacourse.scorderconsumer.dto.OrderDto;
import by.softclub.javacourse.scorderconsumer.service.OrderService;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class KafkaMsgReceiveServiceImpl {

    private final OrderService orderService;

    public KafkaMsgReceiveServiceImpl(OrderService orderService) {
        this.orderService = orderService;
    }

    @KafkaListener(id = "1234", topics = {"softclub"}, containerFactory = "singleFactory")
    public void consume(OrderDto dto) {
        orderService.create(dto);
    }

}

