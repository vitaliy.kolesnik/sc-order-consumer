package by.softclub.javacourse.scorderconsumer.domain.enums;

public enum OrderState {
    CREATED,
    IN_PROCESS,
    FINISHED,
    CANCELED,
    ERROR;
}
